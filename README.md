## SDA Java Advanced Features Playground

Pasi pentru clonarea repository-ului din linia de comanda:

> 1. <code>git clone https://gitlab.com/pistolgeorgiana/sda-java-advanced-features.git</code>
> 
> 2. <code>git branch feature/NumePrenume</code>
>
>> NOTE: git branch va va crea un branch nou pornind de la branch-ul main. Acesta poate fi creat si din interfata pusa la dispozitie de gitlab.
>> IMPORTANT: pentru denumirea noului branch, va rog sa folositi urmatoarea regula, pentru a putea sa le identificam mai usor: feature/NumePrenume.
>
> 3. <code>git checkout feature/NumePrenume</code>

Acum puteti adauga in folderul practice exercitiile realizate in timpul cursului.

In folderul curs, veti putea regasi prezentarile, exercitiile, precum si exemplele de cod prezentate la curs, la fel si exercitiile rezolvate in timpul cursului.

In folderul proiect, o sa regasiti cerintele pentru o mica aplicatie, care nu este obligatorie. Cine doreste sa o rezolve, poate adauga codul sursa in acest folder.

Pentru a incarca materialele pe git, puteti proceda astfel (din linia de comanda, pornind din folderul unde ati clonat repository-ul):

> 1. <code>git checkout feature/NumePrenume</code>
>
> 2. <code>git add .</code>
>
> 3. <code>git status</code>
>> NOTE: cu git status verificati fisierele ce urmeaza a fi comise, nu este obligatorie, dar este utila pentru quick check inainte de commit. Daca nu doriti sa adaugati un anumit tip de fisier (extensii, foldere etc.), il puteti adauga in .gitignore, acesta exista deja, nu il modificati, doar adaugati o noua regula. Specificatiile le puteti gasi aici: https://www.atlassian.com/git/tutorials/saving-changes/gitignore
>
> 4. <code>git commit -m "Orice comentariu sugestiv pentru commit."</code>
>> NOTE: dupa -m, va trebui adaugat un comentariu, de obicei sugestiv, ca sa va ajute pe voi la o mai buna gestionare a versionarii. Acesta poate contine, de exemplu, modificarile efectuate in acest commit.
>
> 5. <code>git push</code>

That's it! Codul tau e acum pe gitlab in repository.

Puteti face asta si direct din IntelliJ, acesta avand VCS integrat, gasiti mai multe detalii aici: https://www.jetbrains.com/help/idea/version-control-integration.html. De preferat ar fi, insa, utilizarea liniei de comanda pentru a va familiariza cu aceste comenzi specifice.