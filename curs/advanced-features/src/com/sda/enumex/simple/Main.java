package com.sda.enumex.simple;

public class Main {

    public static void main(String[] args) {
        Weekday saturday = Weekday.SATURDAY; //instantierea unui enum

        System.out.println("Saturday is holiday: " + saturday.isHoliday());
        System.out.println("Firday is weekday: " + Weekday.FRIDAY.isWeekDay());
        Weekday.TUESDAY.whichIsGreater(Weekday.FRIDAY);
    }
}
