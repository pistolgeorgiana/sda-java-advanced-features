package com.sda.enumex.complex;

public enum PackageSize {

    SMALL(0, 5),
    MEDIUM(5, 10),
    LARGE(10, 20);

    private final int minimumPackageSize;
    private final int maximumPackageSize;

    PackageSize(int minimumPackageSize, int maximumPackageSize) {
        this.minimumPackageSize = minimumPackageSize;
        this.maximumPackageSize = maximumPackageSize;
    }

    static PackageSize getPackageSize(int packageSize) {
        if (SMALL.minimumPackageSize <= packageSize && SMALL.maximumPackageSize >= packageSize) {
            return SMALL;
        }
        if (MEDIUM.minimumPackageSize < packageSize && MEDIUM.maximumPackageSize >= packageSize) {
            return MEDIUM;
        }
        if (LARGE.minimumPackageSize < packageSize && LARGE.maximumPackageSize >= packageSize) {
            return LARGE;
        }
        return null;
    }
}
