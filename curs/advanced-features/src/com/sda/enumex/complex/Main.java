package com.sda.enumex.complex;

public class Main {

    public static void main(String[] args) {
        System.out.println(PackageSize.getPackageSize(3));
        System.out.println(PackageSize.getPackageSize(7));
        System.out.println(PackageSize.getPackageSize(12));
        System.out.println(PackageSize.getPackageSize(23));
        System.out.println(PackageSize.getPackageSize(-5));
    }
}
