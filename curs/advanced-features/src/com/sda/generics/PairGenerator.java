package com.sda.generics;

public class PairGenerator {
    public static <K, V> Pair<K, V> generatePair(K key, V value) {
        Pair<K, V> pair = new Pair<K, V>();
        pair.setKey(key);
        pair.setValue(value);
        return pair;
    }

    public static void main(String[] args) {
        Pair<Integer, String> firstPair = PairGenerator.generatePair(1, "value1");
        Pair<Long, String> secondPair = PairGenerator.<Long, String>generatePair(2L, "value2");

        System.out.println(firstPair);
        System.out.println(secondPair);
    }
}