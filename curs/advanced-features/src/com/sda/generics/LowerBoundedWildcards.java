package com.sda.generics;

import java.util.ArrayList;
import java.util.List;

public class LowerBoundedWildcards {

    public static void main(String[] args) {
        addNumbers(new ArrayList<>(List.of(1, 2, 3)));
        addNumbers(new ArrayList<>(List.of(new Object(), new Object(), new Object())));
    }

    public static void addNumbers(List<? super Integer> list) {
        for (int i = 1; i <= 10; i++) {
            list.add(i);
        }
        System.out.println(list);
    }
}