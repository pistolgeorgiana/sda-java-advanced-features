package com.sda.ioexercises;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static User parserCSV(String line) {
        String[] userData = line.split(",");
        // split > metoda din clasa String care imparte sirul de caractere dupa expresia specificata in
        // paranteza (in cazul de fata, dupa virgula) si returneaza un vector de siruri de caractere
        // in functie de impartirea facuta
        return new User(userData[0], userData[1], Integer.parseInt(userData[2]));
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = null;

        try {
            in = new BufferedReader(new FileReader("users.csv"));
            // cream un obiect de tip BufferedReade si un FileReader pentru citirea
            // fisierului cu java.io > stream buffer
            // pentru cei cu Maven > fisierul trebuie adaugat in src -> java -> resources
            // si new FileReader(Main.class.getResource("/users.csv").getFile()
            String line;

            while ((line = in.readLine()) != null) { // readLine > citire linie cu linie
                System.out.println(parserCSV(line));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
        }

    }
}
