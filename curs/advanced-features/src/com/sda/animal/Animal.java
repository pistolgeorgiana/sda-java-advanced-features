package com.sda.animal;

public abstract class Animal {

    public abstract void eat(int unitsOfFood);

    public abstract void move(int distance);
}