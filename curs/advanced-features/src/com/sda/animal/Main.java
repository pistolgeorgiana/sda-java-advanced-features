package com.sda.animal;

public class Main {

    public static void main(String[] args) {
//        ERROR: nu putem instantia o clasa abstracta

//        Animal animal = new Animal();
//        animal.move(1);

//        Terrestrial terrestrialAnimal = new Terrestrial();
//        terrestrialAnimal.specialCharacteristic();

        Animal rhinoAnimal = new Rhino();
        rhinoAnimal.move(2);

        Terrestrial terrestrialRhino = new Rhino();
        terrestrialRhino.saySomething();

        Rhino rhino = new Rhino();
        rhino.eat(9);
    }
}
