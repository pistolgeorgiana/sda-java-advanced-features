package com.sda.animal;

public abstract class Terrestrial extends Animal {

    public abstract void saySomething();

    public abstract void specialCharacteristic();
}
