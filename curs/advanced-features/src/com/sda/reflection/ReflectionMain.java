package com.sda.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionMain {

    public static void getMethodsAndFields() throws ClassNotFoundException {
        Class<?> carClass = Class.forName("com.sda.reflection.Car"); // getting the Class object for the class Car
        Method[] methods = carClass.getDeclaredMethods();
        Field[] fields = carClass.getDeclaredFields();

        System.out.println("Available methods: ");
        for (Method method : methods) {
            System.out.println(method);
        }

        System.out.println("\nAvailable fields: ");
        for (Field field : fields) {
            System.out.println(field);
        }
    }

    public static Car createInstanceUsingReflection() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Car car = Car.class.getConstructor(String.class, String.class, boolean.class).newInstance("param1", "param2", true);
        System.out.println("\nCreate object using reflection: " + car);

        return car;
    }

    public static void invokeMethodUsingReflection() throws Exception {
        Class<?> carClass = Class.forName("com.sda.reflection.Car"); // getting the Class object for the class Car
        Car car = createInstanceUsingReflection();

        Method setNameMethod = carClass.getDeclaredMethod("setName", String.class);
        Method setModelMethod = carClass.getDeclaredMethod("setModel", String.class);
        Method getNameMethod = carClass.getDeclaredMethod("getName");

        setNameMethod.invoke(car, "Porsche");
        setModelMethod.invoke(car, "K1");

        getNameMethod.setAccessible(true);

        System.out.println("\nGet name: " + getNameMethod.invoke(car));
        System.out.println("\nUse method using reflection: ");
        System.out.println(car);
    }

    public static void setFieldUsingRelection() throws NoSuchFieldException, IllegalAccessException {
        Car car = new Car();

        Field field = Car.class.getDeclaredField("name");
        field.setAccessible(true);
        field.set(car, "test");

        Field modelField = Car.class.getDeclaredField("model");
        modelField.setAccessible(true);
        modelField.set(car, "BMW");

        System.out.println("\nSet field using reflection: " + car);
    }

    public static void main(String[] args) {
        try {
            getMethodsAndFields();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            invokeMethodUsingReflection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            setFieldUsingRelection();
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
