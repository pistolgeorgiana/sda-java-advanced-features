package com.sda.person;

public class Singer extends Person implements IHasName, ICanSing {

    public Singer(String lastName, String firstname, int age) {
        super(lastName, firstname, age); //apeleaza constructorul clasei pe care o mosteneste
        // apelul constructorului cu super trebuie realizat prima data,
        // altfel, un apel ulterior al constructorului cu super nu este permis
    }

    // Note: no need to implement getName() as the method has already
    //       an implementation inherited from class 'Person'

    @Override
    public void sing(String song) {
        System.out.println("I am singing: " + song);
    }
}