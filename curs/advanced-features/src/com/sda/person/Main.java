package com.sda.person;

public class Main {

    public static void main(String[] args) {
        Singer singer = new Singer("Barrett", "Syd", 32);

        IHasName iHasNameRef = singer;
        System.out.println("The rockstar name is: " + iHasNameRef.getName());

//        ICanSing iCanSingRef = iHasNameRef;

        ICanSing iCanSing2Ref = singer;
        iCanSing2Ref.sing("The best");

        ICanSing singer1 = new Singer("Jackson", "Michael", 49);
        singer1.sing("Thriller");
    }
}
