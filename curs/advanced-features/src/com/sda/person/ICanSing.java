package com.sda.person;

public interface ICanSing {

    void sing(String song);
}
