package com.sda.person;

public interface IMovable {

    // static & final fields:
    public double metersToKilometers = .001;

    // abstract methods:
    public void moveForward();
    public void moveLeft();
    public void moveRight();

    double getDistanceWalked();
}