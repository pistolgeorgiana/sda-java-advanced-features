package com.sda.person;

public interface IHasName {

    String getName();
}
