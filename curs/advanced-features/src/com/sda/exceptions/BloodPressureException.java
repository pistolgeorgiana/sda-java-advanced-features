package com.sda.exceptions;

public class BloodPressureException extends RuntimeException {
    public BloodPressureException() {
        super();
    }
    public BloodPressureException(String message) {
        super(message);
    }
}
