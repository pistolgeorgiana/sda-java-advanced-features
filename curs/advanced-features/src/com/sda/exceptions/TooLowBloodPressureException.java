package com.sda.exceptions;

public class TooLowBloodPressureException extends BloodPressureException {
    public TooLowBloodPressureException() {
        super();
    }
    public TooLowBloodPressureException(String message) {
        super(message);
    }
}
