package com.sda.exceptions;

public class CannotDivideBy0Exception extends RuntimeException {

    public CannotDivideBy0Exception(String message) {
        super(message); // apeleaza constructorul din RuntimeException
        // pentru initializarea mesajului erorii
    }
}
