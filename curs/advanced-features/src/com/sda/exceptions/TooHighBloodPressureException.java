package com.sda.exceptions;

public class TooHighBloodPressureException extends BloodPressureException {
    public TooHighBloodPressureException() {
        super();
    }
    public TooHighBloodPressureException(String message) {
        super(message);
    }
}
