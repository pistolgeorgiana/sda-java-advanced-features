package com.sda.exceptions;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Main {

    public static void catchKeyHierarchy() {
        try {
            final KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            final Cipher cipher = Cipher.getInstance("ABS");
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Incorrect algorithm name");
        } catch (NoSuchPaddingException e) {
            System.err.println("Oops");
        } catch (Exception e) {
            System.err.println("Generic exception occurred");
        }
    }

    public static String readFirstLineFromFile(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            return br.readLine();
        } catch (IOException e) {
            return "FAILED";
        }

//        BufferedReader bufferedReader = null;
//        try {
//            bufferedReader = new BufferedReader(new FileReader(path)));
//        } catch (FileNotFoundException e) {
//            System.exit(1);
//        } finally {
//            if (bufferedReader != null) {
//                bufferedReader.close();
//            }
//        }
    }

    public static float divide(int a, int b) {
        if (b == 0) {
            throw new CannotDivideBy0Exception("b is 0!");
        }
        return a / b;
    }

    public static void main(String[] args) {
//        catchKeyHierarchy();
//        readFirstLineFromFile("/tmp/sda.txt");

//        int bloodPressure = Integer.parseInt(args[0]);
//        int bloodPressure = 150;
//        if (bloodPressure > 140) {
//            throw new TooHighBloodPressureException("Elevated blood pressure!");
//        }

        divide(6, 0);
    }
}
