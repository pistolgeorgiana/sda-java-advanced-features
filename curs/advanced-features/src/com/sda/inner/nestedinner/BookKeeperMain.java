package com.sda.inner.nestedinner;

public class BookKeeperMain {

    public static void main(String[] args) {

        Book masterOfTheGame = new Book(463);

        Book.Page page23 = masterOfTheGame.new Page(23);
        Book.Page page3 = masterOfTheGame.getPage(3);

//        ERROR: constructorul nu poate fi apelat fara a avea un obiect initializat > clasa inner
//        Constructorul se apeleaza sub forma obiect.new InnerClass()

//        Book.Page page2 = new Page(23);
//        Book.Page page100 = new masterOfTheGame.Page(23);
//        Book.Page page1 = Book.new Page(23);
//        Book.Page page1 = new Book.Page(23);

        System.out.println(page3.getText());
    }
}
