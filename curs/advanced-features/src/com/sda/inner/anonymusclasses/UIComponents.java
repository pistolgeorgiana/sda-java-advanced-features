package com.sda.inner.anonymusclasses;

public class UIComponents {
    void showComponents() {
        // an anonymous class implementation using the new keyword
        ClickListener buttonClick = new ClickListener() {
            @Override
            public void onClick() {
                System.out.println("On Button click!");
            }
        };
        // end of anonymous class implementation

//        cum face Java in spate pentru a defini o clasa anonima
//        class Anonym implements ClickListener {
//            @Override
//            public void onClick() {
//                System.out.println("On Button click!");
//            }
//            @Override
//            public void onDoubleClick() {
//                System.out.println("On Double click!");
//            }
//        }

        buttonClick.onClick();
    }

    void showComponentsV2() {
        // anonymous class implementare
        ClickListener buttonClick = new ClickListener() {

            private String name; // atribut in clasa anonima
            private static final String BUTTON_CLICK_MESSAGE ="On Button click!"; // atribut static in clasa anonima

            public void sayHello() { // implementarea unei metode in clasa anonima
                System.out.println("I am new method in anonymous class");
            }

            @Override
            public void onClick() {
                sayHello();
                System.out.println(BUTTON_CLICK_MESSAGE);
            }
        };
    }
}
