package com.sda.inner.staticclasses;

public class MainEclipseStatic {

    public static void main(String[] args) {
        EclipseIDE eclipse = new EclipseIDE("/Users/Eclipse");

        // Remember inner non-static class initialization:
        EclipseIDE.FilePath filePath = eclipse.new FilePath();

        EclipseIDE.WorkspaceName workspaceName = new EclipseIDE.WorkspaceName("javaWorkspace");

        //EclipseIDE.WorkspaceName workspaceName2 = eclipse.new WorkspaceName("javaWorkspace");

        System.out.println(filePath.getFilePath("InnerClasses.java"));
        System.out.println(workspaceName.getWorkspaceName());
    }
}
