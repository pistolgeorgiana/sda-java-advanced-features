package com.sda.exceptionsex;

public class BookAlreadyExistsException extends Exception {
    // daca extinde Exception, exceptia trebuie neaparat tratata
    // tratata > throws / try catch
    public BookAlreadyExistsException() {
    }

    public BookAlreadyExistsException(String message) {
        super(message);
    }

    public BookAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
