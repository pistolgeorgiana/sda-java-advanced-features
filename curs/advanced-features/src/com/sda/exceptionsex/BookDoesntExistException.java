package com.sda.exceptionsex;

public class BookDoesntExistException extends RuntimeException {
    // daca extinde RuntimeException, exceptia este unchecked, deci nu trebuie neaparat tratata
    // tratata > throws / try catch
    public BookDoesntExistException(String message) {
        super(message);
    }
}
