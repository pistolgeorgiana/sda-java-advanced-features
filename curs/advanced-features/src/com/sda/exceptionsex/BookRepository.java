package com.sda.exceptionsex;

public class BookRepository {

    private Book[] books = new Book[10]; // alocarea unui vector cu 10 elemente, iitializate cu null

    public void addBook(Book newBook) throws BookAlreadyExistsException {
        // throws in semnatura functiei este necesar pentru ca BookAlreadyExistsException extinde Exception
        // ceea ce inseamna ca trebuie tratata prin try catch sau propagata (throws)
        for (int i = 0; i < books.length; i++) {
            if (books[i] == null) {
                books[i] = newBook;
                return;
            } else if (newBook.getTitle().equals(books[i].getTitle())) {
                throw new BookAlreadyExistsException("Book " + newBook.getTitle() + " already exists!");
            }
        }
    }

    public Book searchBook(String idBook) {
        for (Book book : books) {
            if (book != null && idBook.equals(book.getId())) {
                return book;
            }
        }
        throw new BookDoesntExistException("Book with id " + idBook + " doesn't exist!");
    }
}
