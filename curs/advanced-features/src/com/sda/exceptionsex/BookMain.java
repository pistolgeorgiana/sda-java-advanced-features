package com.sda.exceptionsex;

public class BookMain {

    public static void main(String[] args) throws BookAlreadyExistsException {
        BookRepository bookRepository = new BookRepository();
        Book javaBook = new Book("1", "Java Essentials", "SDA", 2022);
        Book pythonBook = new Book("2", "Python Essentials", "SDA", 2023);
        Book newJavaBook = new Book("3", "Java Essentials", "SDA", 2023);

        try {
            bookRepository.addBook(javaBook);
            bookRepository.addBook(pythonBook);
            bookRepository.addBook(newJavaBook);
        } catch (BookAlreadyExistsException e) {
            System.out.println(e.getMessage());
        }

        try {
            System.out.println(bookRepository.searchBook("1"));
            System.out.println(bookRepository.searchBook("2"));
            System.out.println(bookRepository.searchBook("3"));
        } catch (BookDoesntExistException e) {
            System.out.println(e.getMessage());
        }
    }
}
