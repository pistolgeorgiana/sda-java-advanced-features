package com.sda.annotations;

public @interface Antet {
    String autor();
    String data();
    String ultimaModificare() default "N/A";

    // Note use of array
    String[] revieweri();
}
