package com.sda.threads;

public class HelloWorldRunnableThread implements Runnable {

    @Override
    public void run() {
        System.out.println("Hello World from another Thread");
        System.out.println(Thread.currentThread().getId());
    }
}
