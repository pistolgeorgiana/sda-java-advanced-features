package com.sda.threads;

public class InterruptMain {
    public static void main(String[] args) {
        Thread sleepingThread = new Thread(new SleepingThread());
        sleepingThread.start();
//        sleepingThread.interrupt(); // sending a stop request

//        Thread sleepingInterruptedThread = new Thread(new SleepingInterruptedThread());
//        sleepingInterruptedThread.start();
//        sleepingInterruptedThread.interrupt(); // sending a stop request
    }
}
