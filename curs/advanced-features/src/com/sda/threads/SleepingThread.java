package com.sda.threads;

public class SleepingThread implements Runnable {

    @Override
    public void run() {
        System.out.println("I will go to sleep");
        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) { // catching an InterruptedException if an interrupt signal was sent while the sleep method is executing
            System.out.println("I was interrupted during sleep");
        }
        System.out.println("I am exiting");
    }
}