package com.sda.threads;

import java.util.concurrent.Callable;

public class ThreadsWaitNotifyExample {
    public static void main(String[] args) throws InterruptedException {
        final Customer customer = new Customer();
        final Thread withDrawThread = new Thread(new WithdrawThread(customer));
        final Thread depositThreadA = new Thread(new DepositThread(customer));
        final Thread depositThreadB = new Thread(new DepositThread(customer));

        withDrawThread.start();
        depositThreadA.start();
        depositThreadB.start();
    }
}

class Customer {
    private int availableAmount = 0;

    public int getAvailableAmount() {
        return availableAmount;
    }

    synchronized void withdraw(int amountToWithdraw) {
        System.out.println("Trying to withdraw " + amountToWithdraw + " RON");
        while (availableAmount < amountToWithdraw) {
            System.out.println("Not enough money! Waiting for transfer!");
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("Oops");
            }
        }
        System.out.println("Withdraw successful!");
    }

    synchronized void deposit(final int amountToDeposit) {
        System.out.println("Depositing " + amountToDeposit + " RON");
        availableAmount += amountToDeposit;
        notify();
    }
}

class WithdrawThread implements Runnable {

    private final Customer customer;

    WithdrawThread(final Customer customer) {
        this.customer = customer;
    }


    @Override
    public void run() {
        customer.withdraw(1000);
    }
}

//class DepositThread implements Callable<Integer> {
class DepositThread implements Runnable {
    private final Customer customer;

    DepositThread(final Customer customer) {
        this.customer = customer;
    }
//
//    @Override
//    public Integer call() throws Exception {
//        customer.deposit(500);
//        return customer.getAvailableAmount();
//    }

    @Override
    public void run() {
        customer.deposit(500);
    }
}
