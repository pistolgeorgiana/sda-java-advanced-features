package com.sda.excollections;

import java.util.*;

public class MainCollections {

    public static void main(String[] args) {
        // HashSet
        Set<Integer> numbersSet = new HashSet<>();
        System.out.println(numbersSet.isEmpty()); // true, Set does not contain elements
        numbersSet.add(1);
        numbersSet.add(17);
        numbersSet.add(3);
        numbersSet.add(2);
        numbersSet.add(1); // Adding an item with a value that already exists - the item is NOT added again
        numbersSet.forEach(System.out::println);
//        for(Integer numberSet : numbersSet) {
//            System.out.println(numberSet);
//        }
        System.out.println("----------------------------------------------");

        // TreeSet
        Set<Integer> numbersTreeSet = new TreeSet<>();
        numbersTreeSet.add(1);
        numbersTreeSet.add(3);
        numbersTreeSet.add(2);
        numbersTreeSet.add(1); // Adding an item with a value that already exists - the item is NOT added again
        numbersTreeSet.forEach (System.out::println);
        System.out.println("----------------------------------------------");

        // LinkedHashSet
        Set<Integer> numbersLinkedSet = new LinkedHashSet<>();
        numbersLinkedSet.add(1);
        numbersLinkedSet.add(3);
        numbersLinkedSet.add(2);
        numbersLinkedSet.add(1); // Adding an item with a value that already exists - the item is NOT added again
        numbersLinkedSet.forEach (System.out::println);
        System.out.println("----------------------------------------------");

        // ArrayList
        List<String> names = new ArrayList<>();
        names.add("Andrew");  // adding an item to the end of the list
        names.add("Gregory"); // adding an item to the end of the list
        names.add("Gigel"); // adding an item to the end of the list
        for (String name: names) {
            System.out.println(name); // Andrew, Gregory will be displayed on the screen, keeping the order
        }
        System.out.println("----------------------------------------------");

        // LinkedList
        List<String> namesLinkedList = new LinkedList<>();
        namesLinkedList.add(0, "Andrew");  // adding an item to the top of the list
        namesLinkedList.add(0, "Gregory"); // adding an item to the top of the list
        for (String name: namesLinkedList) {
            System.out.println(name); // Gregory, Andrew will be printed on the screen in order
        }
        System.out.println("----------------------------------------------");

        // Queue - LinkedList
        Queue<String> events = new LinkedList<>();
        events.offer("ButtonClicked");
        events.offer("MouseMoved");
        System.out.println(events.peek());   // displaying the first element
        System.out.println(events.poll());   // removing the first item from the queue and returning a value
        System.out.println(events.poll());   // removing the first item from the queue again and returning the value
        System.out.println(events.isEmpty()); // at this point the queue is empty
        System.out.println("----------------------------------------------");

        // Dequeue
        Deque<Integer> deque = new ArrayDeque<>();
        // add elements to deque
        deque.offerLast(2);
        deque.offerFirst(1);
        System.out.println (deque.pollLast()); // remove elements from deque along with removing from structure -> 2
        System.out.println (deque.peekLast()); // remove elements from deque without removing them from structure -> 1
        System.out.println("----------------------------------------------");

        // HashMap
        Map<Integer, String> ageToNames = new HashMap<>();
        ageToNames.put(11, "Andrew"); // adding items
        ageToNames.put(22, "Michael");  // adding another pair
        ageToNames.put(33, "John");  // adding a third pair to the map
        ageToNames.remove(22);     // removing an item based on the key
        System.out.println(ageToNames.get(11)); // displaying the value based on the key 11 -> Andrew
        System.out.println("----------------------------------------------");

        // LinkedHashMap
        Map<Integer, String> ageToNamesLinked = new LinkedHashMap<>();
        ageToNamesLinked.put(20, "Maggie");
        ageToNamesLinked.put(40, "Kate");
        ageToNamesLinked.put(30, "Anne");

        for (Integer key : ageToNamesLinked.keySet()) { // key iteration using keySet()
            System.out.println("Key is map: " + key);     // the order of the keys is always the same -> 20, 40, 30
        }

        for (String value : ageToNamesLinked.values()) {   // iteration over values using values()
            System.out.println("Value in map is: " + value); // the order of the values is always the same -> Maggie, Anne, Kate
        }

        for (Map.Entry<Integer, String> ageToName : ageToNamesLinked.entrySet()) { // iteration over pairs with entrySet()
            System.out.println("Value for key  " + ageToName.getKey() + " is " + ageToName.getValue());
        }
        System.out.println("----------------------------------------------");

        // TreeMap
        Map<Integer, Integer> numberToDigitsSum = new TreeMap<>();
        numberToDigitsSum.put(33, 6);
        numberToDigitsSum.put(19, 10);
        numberToDigitsSum.put(24, 6);
        numberToDigitsSum.forEach((key, value) -> System.out.println(key + " " + value));
//        for (Map.Entry<Integer, Integer> entry : numberToDigitsSum.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }
    }
}
