package com.sda.enums;

public enum JsonSerializationStrategyExtra implements IdProvider {
    SNAKE_CASE("snake case"),
    CAMEL_CASE("camel case", "1"),
    KEBAB_CASE("kebab case", "2");

    private final String readableName;
    private final String id;

    JsonSerializationStrategyExtra(final String readableName) {
        this.readableName = readableName;
        this.id = "0";
    }

    JsonSerializationStrategyExtra(final String readableName, final String id) {
        this.readableName = readableName;
        this.id = id;
    }

    public String getReadableName() {
        return readableName;
    }

    @Override
    public String getId() {
        return id;
    }
}