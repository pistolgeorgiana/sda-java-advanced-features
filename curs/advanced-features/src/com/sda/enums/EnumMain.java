package com.sda.enums;

import java.util.stream.Stream;

public class EnumMain {

    public static void main(String[] args) {
        JsonSerializationStrategy strategyA = JsonSerializationStrategy.CAMEL_CASE;

        JsonSerializationStrategy strategyB = JsonSerializationStrategy.CAMEL_CASE;
        System.out.println(strategyA == strategyB); // true
        System.out.println(strategyA.equals(strategyB)); // true

        Stream.of(JsonSerializationStrategy.values()).forEach(System.out::println); // SNAKE_CASE CAMEL_CASE KEBAB_CASE
        System.out.println(JsonSerializationStrategy.valueOf("SNAKE_CASE") == JsonSerializationStrategy.SNAKE_CASE); // true
        System.out.println(JsonSerializationStrategy.CAMEL_CASE.name()); // CAMEL_CASE
        System.out.println(JsonSerializationStrategyExtra.CAMEL_CASE.name()); // CAMEL_CASE
    }
}
