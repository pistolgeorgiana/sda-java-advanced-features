package com.sda.enums;

public enum JsonSerializationStrategy {
    SNAKE_CASE,
    CAMEL_CASE,
    KEBAB_CASE
}
