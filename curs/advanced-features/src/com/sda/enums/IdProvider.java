package com.sda.enums;

public interface IdProvider {

    String getId();
}
