package com.sda.iooperations;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializableMovieMain {

    public static void main(String[] args) {
        try {
            Movie movieToSerialize = new Movie(1123, "Star Wars", "Start Wars IX");
            FileOutputStream fileOutputStream = new FileOutputStream("movies.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(movieToSerialize);
            out.flush();
            out.close();
        } catch (Exception e) {
            System.err.println(e);
        }

        try {
            FileInputStream fileIn = new FileInputStream("movies.txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Movie movieToDeserialize = (Movie) in.readObject();
            in.close();
            fileIn.close();
            System.out.println(movieToDeserialize);
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
