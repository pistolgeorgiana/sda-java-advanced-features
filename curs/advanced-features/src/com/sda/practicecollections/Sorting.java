package com.sda.practicecollections;

import java.util.Arrays;

public class Sorting {

    public static void main(String[] args) {
        UserComparable[] users = new UserComparable[]{
                new UserComparable("Peter", 40),
                new UserComparable("John", 23)
        };
        Arrays.sort(users); // This method uses an implementation of the Comparable interface to sort the array
        System.out.println(Arrays.toString(users)); // output: [User{name='John', age=23}, User{name='Peter', age=40}]
    }
}

class UserComparable implements Comparable<UserComparable>{
    private String name;
    private int age;

    public UserComparable(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(UserComparable o) {
        return  this.getAge() - o.age;
    }
}
