package com.sda.exercises;

public abstract class Shape {

    protected String text;
    protected String material;

    public Shape() {
    }

    public Shape(String text, String material) {
        this.text = text;
        this.material = material;
    }

    public abstract void displayHeight();

    public double getSize() {
        return -1;
    }

    @Override
    public String toString() {
        return "made of " + material + ", contains the text: " + text + '.';
    }
}
