package com.sda.exercises;

public interface IEditable {

    void updateText(String text);

    void changeMaterial(String material);
}
