package com.sda.exercises;

public class Rectangle extends Shape implements IResizable {

    private int height;
    private int width;

    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public Rectangle(String text, String material, int height, int width) {
        super(text, material);
        this.height = height;
        this.width = width;
    }

//    public void displayRectangleHeight() {
//        System.out.println("Rectangle height is: " + this.height);
//    }

    @Override
    public void displayHeight() {
        System.out.println("Rectangle height is: " + this.height);
    }

    @Override
    public double getSize() {
        // return super.getSize(); // explicatioa in cadrul clasei Triangle
        return this.height * this.width;
    }

    @Override
    public void resize(int size) {
        this.height = this.height + size;
        this.width = this.width + size;
    }

    @Override
    public String toString() {
        return "Rectangle: height is " + height + ", width is: " + width + ", " + super.toString();

    }
}
