package com.sda.exercises;

public class Triangle extends Shape implements IResizable, IEditable {

    private int height;
    private int base;

    public Triangle(int height, int base) {
        this.height = height;
        this.base = base;
    }

    public Triangle(String text, String material, int height, int base) {
        super(text, material);
        this.height = height;
        this.base = base;
    }

//    public void displayTriangleHeight() {
//        System.out.println("Triangle height is: " + this.height);
//    }

    @Override
    public void displayHeight() {
        System.out.println("Triangle height is: " + this.height);
    }

    //    @Override
    public double getSize() {
        // return super.getSize(); // > returneaza -1
        // super are acelasi rol ca in cazul contructorului
        // apeleaza metoda din clasa pe care o extinde > Shape
        // comparativ cu super din cazul constructorului, in cazul metodei,
        // super poate fi apelat oricand, nu doar prima data
        return (this.base * this.height) / 2D; //D-ul este optional, ajuta la conversia fara "pierderi" catre double
    }

    @Override
    public void updateText(String text) {
        this.text = text;
    }

    @Override
    public void changeMaterial(String material) {
        this.material = material;
    }

    @Override
    public void resize(int size) {
        this.height -= size;
        this.base -= size;
    }

    @Override
    public String toString() {
        return "Triangle: height is " + height + ", base is: " + base + ", " + super.toString();
    }
}
