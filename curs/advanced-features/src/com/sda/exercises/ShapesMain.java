package com.sda.exercises;

public class ShapesMain {

    public static void main(String[] args) {
//        Shape shape = new Shape(); // ERROR: nu se poate instantia o clasa abstracta

        Triangle triangle = new Triangle(23, 12);
        Triangle triangleShape = new Triangle("triangle", "plastic", 10, 9);
        Shape tShape = triangle;

        triangle.displayHeight();
        System.out.println("Triangle size: " + triangle.getSize());
        System.out.println("TShape size: " + tShape.getSize());

        triangleShape.displayHeight();
        System.out.println("TriangleShape size: " + triangleShape.getSize());

        Rectangle rectangle = new Rectangle(8, 4);
        Rectangle rectangleShape = new Rectangle("rectangle", "iron", 9, 13);

        rectangle.displayHeight();
        System.out.println("Rectangle size: " + rectangle.getSize());

        rectangleShape.displayHeight();
        System.out.println("RectangleShape size: " + rectangleShape.getSize());

        // interfete
        rectangle.resize(2);
        System.out.println(rectangle);

        triangle.updateText("Triangle shape");
        triangle.changeMaterial("carbon");
        triangle.resize(1);
        System.out.println(triangle);
    }
}
