package com.sda.exercises;

public interface IResizable {

    void resize(int size);
}
