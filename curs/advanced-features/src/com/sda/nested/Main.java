package com.sda.nested;

public class Main {

    public static void main(String[] args) {
        Movie movie = new Movie.MovieCreator() //apelul constructorului clasei statice interne MovieCreator
                .setTitle("Star Wars") // returneaza acelasi obiect de tipul MovieCreator, deci putem sa apelam alte metode din cadrul clasei
                .setDirector("J.J Abrams")
                .setGenre("Action")
                .setYearOfRelease(2015)
                .setPublisher("Disney")
                .createMovie();

        System.out.println(movie); // toString() este suprascrisa, mostenita din Object
    }
}
