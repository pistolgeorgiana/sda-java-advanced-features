package com.sda.oop.abstraction;

public abstract class DataParser { // abstract class

    protected String data; // the protected modifier allows the use of a field in derived classes

    public abstract Data parse();

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void validateData() {
        if(data == null || data.isEmpty()) {
            throw new IllegalArgumentException("data are not valid!");
        }
    }
}