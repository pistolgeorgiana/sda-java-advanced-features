package com.sda.oop.polymorphism;

public abstract class VodPlayer {
    public abstract void play(String title);
}
