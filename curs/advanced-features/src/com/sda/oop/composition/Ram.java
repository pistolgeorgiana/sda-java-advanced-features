package com.sda.oop.composition;

public class Ram {

    private String name;
    private int size;

    public Ram(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }
}
