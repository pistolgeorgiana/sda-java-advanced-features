package com.sda.oop.encapsulation;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Employee employee = new Employee("1", "Gigel", "Popescu", LocalDate.of(1980, 2, 10));

        System.out.println(employee.getDateOfBirth());
        System.out.println(employee.getFirstName());
        System.out.println(employee.getLastName());
        System.out.println(employee.getId());

        // We cannot change the id or birthdate of the employee because no setter has been defined therefore we have read-only access
        //employee.setId("3");
        //employee.setDateOfBirth(LocalDate.now());

        // But we can change the employee's name
        employee.setFirstName("Ionel");
        employee.setLastName("Pop");

        System.out.println(employee.getFirstName());
        System.out.println(employee.getLastName());

        System.out.println(employee);
    }
}
